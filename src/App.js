import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import './App.css';

import AppNavbar from './components/AppNavbar'
import Home from './pages/Home';



function App() {

  return(
    <Fragment>
      <AppNavbar />
      <Container>
      <Home />
      </Container>
    </Fragment>
  )

}

export default App;
